//
//  CppObject.h
//  ReactNativeDemoApp
//
//  Created by Gabriel Chan on 14/8/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef CppObject_h
#define CppObject_h

#include <string>
class CppObject
{
public:
    int squareMe(const std::string& number);
};

#endif /* CppObject_h */
