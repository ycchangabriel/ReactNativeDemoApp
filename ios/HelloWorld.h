//
//  HelloWorld.h
//  ReactNativeDemoApp
//
//  Created by Gabriel Chan on 13/8/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef HelloWorld_h
#define HelloWorld_h

#import "React/RCTBridgeModule.h"
#import "CppObject.h"

#if defined __cplusplus
class CppObject;                    // forward declaration
#else
typedef struct CppClass CppClass;   // forward struct declaration
#endif

@interface HelloWorld : NSObject <RCTBridgeModule> {
@private
  CppObject* cppObject;
}

@end


#endif /* HelloWorld_h */
