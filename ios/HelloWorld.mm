//
//  HelloWorld.m
//  ReactNativeDemoApp
//
//  Created by Gabriel Chan on 13/8/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import "HelloWorld.h"

@implementation HelloWorld

// The React Native bridge needs to know our module
RCT_EXPORT_MODULE()

- (NSDictionary *)constantsToExport {
    return @{@"greeting": @"Welcome to the DevDactic\n React Native Tutorial!"};
}

RCT_EXPORT_METHOD(squareMe:(NSString *)number:(RCTResponseSenderBlock)callback) {
    int num = [number intValue];
    callback(@[[NSNull null], [NSNumber numberWithInt:(num*num)]]);
}

- (NSInteger)squareMe:(NSString *)number {
    int squared = cppObject->squareMe(std::string([number UTF8String]));
    return (NSInteger)squared;
}

@end
