#include <string>
#include "CppObject.h"

int CppObject::squareMe(const std::string& number)
{
    int num = std::stoi(number);
    return num*num;
}
