package com.reactnativedemoapp.demo;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;

public class HelloWorldModule extends ReactContextBaseJavaModule {
    static {
        System.loadLibrary("hello_world_jni"); //this loads the library when the class is loaded
    }

    public HelloWorldModule(ReactApplicationContext reactContext) {
        super(reactContext); //required by React Native
    }

    @Override
    public String getName() {
        return "HelloWorld"; //HelloWorld is how this module will be referred to from React Native
    }

    @ReactMethod
    public void helloWorld(ReadableMap structMap, double num, Promise promise) { //this method will be called from JS by React Native
        try {
            MyStruct myStruct = new MyStruct(structMap);
            MyStruct newStruct = helloStructJNI(myStruct, num);
            promise.resolve(newStruct.toWritableMap());
        } catch (Exception e) {
            promise.reject("ERR", e);
        }
    }

    public native String helloWorldJNI();
    public native MyStruct helloStructJNI(MyStruct struct, double d);
}
