LOCAL_PATH := $(call my-dir)

# hello world test module
include $(CLEAR_VARS)
LOCAL_MODULE := hello_world_jni
LOCAL_SRC_FILES := hello_world.c
include $(BUILD_SHARED_LIBRARY)

# c++ super simple math module
include $(CLEAR_VARS)
LOCAL_MODULE := math
LOCAL_SRC_FILES := math/math.cpp
APP_CPPFLAGS += -std=c++11
include $(BUILD_SHARD_LIBRARY)
